package com.example.appholamundo;

import android.os.Bundle;
import android.view.*;
import android.widget.*;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblNombre, lblNumRecibo, lblSubtotal, lblImpuesto, lblTotal;
    private EditText txtHorasTrabajadas, txtHorasExtras;
    private RadioButton rdbAuxiliar, rdbAlbañil, rdbIngObra;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);

        lblNombre = findViewById(R.id.lblNombre);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        rdbAlbañil = findViewById(R.id.rdbAlbañil);
        rdbIngObra = findViewById(R.id.rdbIngObra);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        String nombre = getIntent().getStringExtra("NOMBRE_TRABAJADOR");
        lblNombre.setText("Nombre: "+nombre);
        String numeroR= getIntent().getStringExtra("numRecibo");
        lblNumRecibo.setText("Num. Recibo: "+numeroR);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularNomina();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    private void calcularNomina() {
        // Validar entradas
        if (txtHorasTrabajadas.getText().toString().isEmpty() || txtHorasExtras.getText().toString().isEmpty()) {
            Toast.makeText(this, "Ingrese las horas trabajadas y extras", Toast.LENGTH_SHORT).show();
            return;
        }

        int horasTrabajadas;
        int horasExtras;
        try {
            horasTrabajadas = Integer.parseInt(txtHorasTrabajadas.getText().toString());
            horasExtras = Integer.parseInt(txtHorasExtras.getText().toString());
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese valores válidos para las horas", Toast.LENGTH_SHORT).show();
            return;
        }

        float salarioBase = 200;
        float salarioHora = 0;

        // Determinar el salario por hora basado en el puesto
        if (rdbAuxiliar.isChecked()) {
            salarioHora = salarioBase * 1.2f; // Auxiliar: base + 20%
        } else if (rdbAlbañil.isChecked()) {
            salarioHora = salarioBase * 1.5f; // Albañil: base + 50%
        } else if (rdbIngObra.isChecked()) {
            salarioHora = salarioBase * 2.0f; // Ingeniero de Obra: base + 100%
        } else {
            Toast.makeText(this, "Seleccione un puesto", Toast.LENGTH_SHORT).show();
            return;
        }

        // Calcular subtotal
        float subtotal = (horasTrabajadas * salarioHora) + (horasExtras * salarioHora * 2);
        float impuesto = subtotal * 0.16f;  // 16% de impuesto
        float total = subtotal - impuesto;

        // Actualizar los TextView con los resultados
        lblSubtotal.setText(String.format("%.2f", subtotal));
        lblImpuesto.setText(String.format("%.2f", impuesto));
        lblTotal.setText(String.format("%.2f", total));
    }

    private void limpiarCampos() {
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        lblSubtotal.setText("");
        lblImpuesto.setText("");
        lblTotal.setText("");
        rdbAuxiliar.setChecked(false);
        rdbAlbañil.setChecked(false);
        rdbIngObra.setChecked(false);
    }
}