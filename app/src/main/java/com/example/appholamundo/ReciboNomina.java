package com.example.appholamundo;

import java.util.Random;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horaTrabNormal;
    private float horaTrabExtra;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horaTrabNormal, float horaTrabExtra, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horaTrabNormal = horaTrabNormal;
        this.horaTrabExtra = horaTrabExtra;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }
    public ReciboNomina() {
        this.numRecibo =0;
        this.nombre = "";
        this.horaTrabNormal =0.0f;
        this.horaTrabExtra = 0.0f;
        this.puesto = 0;
        this.impuestoPorc = 0.0f;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHoraTrabNormal() {
        return horaTrabNormal;
    }

    public void setHoraTrabNormal(float horaTrabNormal) {
        this.horaTrabNormal = horaTrabNormal;
    }

    public float getHoraTrabExtra() {
        return horaTrabExtra;
    }

    public void setHoraTrabExtra(float horaTrabExtra) {
        this.horaTrabExtra = horaTrabExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }
    private static final float PAGO_BASE = 200.0f;
    public float calcularSubtotal() {
        float pagoPorHora = PAGO_BASE;
        switch (puesto) {
            case 1:
                pagoPorHora += PAGO_BASE * 0.2;
                break;
            case 2:
                pagoPorHora += PAGO_BASE * 0.5;
                break;
            case 3:
                pagoPorHora += PAGO_BASE * 1.0;
                break;
        }
        return (horaTrabNormal * pagoPorHora) + (horaTrabExtra * pagoPorHora * 2);
    }

    public float calcularImpuesto() {
        return calcularSubtotal() * (impuestoPorc / 100);
    }

    public float calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }
    public int generaNumRecibo() {
        Random r = new Random();
        return r.nextInt(1000);
    }

}
