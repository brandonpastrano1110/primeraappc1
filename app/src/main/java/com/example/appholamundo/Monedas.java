package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class Monedas extends AppCompatActivity {
    private EditText etxtCantidad;
    private TextView txtResultado;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    private Spinner spnTipo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_monedas);
        iniciarComponentes();

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Spinner spnTipo = findViewById(R.id.spnTipo);
        String[] items = {"MXN a USD", "MXN a CAD", "MXN a EUR", "MXN a GBP"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTipo.setAdapter(adapter);

        spnTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Obtener el elemento seleccionado
                String selectedItem = parent.getItemAtPosition(position).toString();
                // Mostrar un mensaje con el elemento seleccionado
                Toast.makeText(Monedas.this, "Seleccionaste: " + selectedItem, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularCambio();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etxtCantidad.setText("");
                txtResultado.setText("");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Monedas.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private void calcularCambio() {
        String cantidadStr = etxtCantidad.getText().toString();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);
        String tipoCambio = spnTipo.getSelectedItem().toString();
        double resultado = 0;

        switch (tipoCambio) {
            case "MXN a USD":
                resultado = cantidad / 16.69;
                break;
            case "MXN a CAD":
                resultado = cantidad / 12.22;
                break;
            case "MXN a EUR":
                resultado = cantidad / 18.11;
                break;
            case "MXN a GBP":
                resultado = cantidad / 21.26;
                break;
            default:
                Toast.makeText(this, "Tipo de cambio no selecciondo", Toast.LENGTH_SHORT).show();
                return;
        }
        txtResultado.setText(String.format("Resultado: %.2f", resultado));
    }

    public void iniciarComponentes(){
        etxtCantidad = (EditText) findViewById(R.id.etxtCantidad);
        txtResultado=(TextView) findViewById(R.id.txtResultado);
        btnCalcular=(Button) findViewById(R.id.btnCalcular);
        btnCerrar=(Button) findViewById(R.id.btnCerrar);
        btnLimpiar=(Button) findViewById(R.id.btnLimpiar);
        spnTipo=(Spinner) findViewById(R.id.spnTipo);
    }
}