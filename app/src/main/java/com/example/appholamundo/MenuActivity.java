package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {
    private CardView crvPrimer, crvImc, crvCambio, crvConversion, crvCotizacion, crvSpinner;
    private TextView txtAlumno, txtMateria, txtMatricula;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();
        //codificar los elementos clic de las tarjetas
        crvPrimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ReciboNominaMain.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Imc.class);
                startActivity(intent);
            }
        });
        crvConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Conversion_grados.class);
                startActivity(intent);
            }
        });
        crvCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Monedas.class);
                startActivity(intent);
            }
        });
        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, cotizacionMainActivity.class);
                startActivity(intent);
            }
        });
        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        txtAlumno = (TextView) findViewById(R.id.txtAlumno);
        txtMatricula = (TextView) findViewById(R.id.txtMatricula);
        txtMateria = (TextView) findViewById(R.id.txtMateria);
        crvPrimer = (CardView) findViewById(R.id.crvHola);
        crvImc = (CardView) findViewById(R.id.crvImc);
        crvCambio = (CardView) findViewById(R.id.crvMoneda);
        crvConversion = (CardView) findViewById(R.id.crvConversiones);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        crvSpinner= (CardView) findViewById(R.id.crvSpinner);
    }
}