package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaMain extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnEntrar;

    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina_main);
        ReciboNomina recibo = new ReciboNomina();
        // Inicializar vistas
        txtNombre = findViewById(R.id.txtNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
        btnEntrar.setOnClickListener(v -> {
            String nombre = txtNombre.getText().toString();
            String numRecibo= String.valueOf(recibo.generaNumRecibo());
            Intent intent = new Intent(ReciboNominaMain.this, ReciboNominaActivity.class);
            intent.putExtra("NOMBRE_TRABAJADOR", nombre);
            intent.putExtra("numRecibo", numRecibo);
            startActivity(intent);
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ReciboNominaMain.this, MenuActivity.class);
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}