package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class Imc extends AppCompatActivity {
    private EditText etxtNombre, etxtEdad, etxtPeso, etxtAltura;
    private TextView trtNombre, trtImc, trtClasificacion;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imc);
        IniciarComponentes();


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float peso;
                int altura;
                try {
                    peso=Float.parseFloat(etxtPeso.getText().toString());
                    altura=Integer.parseInt(etxtAltura.getText().toString());
                }catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Error en los valores de peso o altura", Toast.LENGTH_SHORT).show();
                    return;
                }
                String nombre=etxtNombre.getText().toString();

                if(nombre.isEmpty() || peso <= 0 || altura <= 0){
                    Toast.makeText(getApplicationContext(),"Captura Todos los Datos", Toast.LENGTH_SHORT).show();
                }
                else{
                    trtNombre.setText(etxtNombre.getText().toString());
                    float alturaM=altura/100.0f;
                    float imcCalculo= (peso)/(alturaM*alturaM   );
                    trtImc.setText(String.format("%.2f", imcCalculo));
                    if (imcCalculo < 18.5) {
                        trtClasificacion.setText("Insuficiente - Ponderal");
                    } else if (imcCalculo >= 18.5 && imcCalculo < 25) {
                        trtClasificacion.setText("Normal");
                    } else if (imcCalculo >= 25 && imcCalculo < 30) {
                        trtClasificacion.setText("Sobrepeso");
                    } else if (imcCalculo >= 30 && imcCalculo < 35) {
                        trtClasificacion.setText("Obesidad I");
                    } else if (imcCalculo >= 35 && imcCalculo < 40) {
                        trtClasificacion.setText("Obesidad II");
                    } else if (imcCalculo >= 40) {
                        trtClasificacion.setText("Obesidad III");
                    }
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etxtNombre.setText("");
                etxtEdad.setText("");
                etxtPeso.setText("");
                etxtAltura.setText("");
                trtClasificacion.setText("");
                trtNombre.setText("");
                trtImc.setText("");
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Imc.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void IniciarComponentes(){
        etxtNombre = (EditText) findViewById(R.id.etxtNombre);
        etxtEdad = (EditText) findViewById(R.id.etxtEdad);
        etxtPeso = (EditText) findViewById(R.id.etxtPeso);
        etxtAltura = (EditText) findViewById(R.id.etxtAltura);
        //etxtResultado = (EditText) findViewById(R.id.etxtResultado);
        trtClasificacion = (TextView) findViewById(R.id.trtClasificacion);
        trtNombre = (TextView) findViewById(R.id.trtNombre);
        trtImc = (TextView) findViewById(R.id.trtImc);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

    }
}