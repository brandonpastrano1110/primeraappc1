package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {
    private TextView txtUsuario, txtFolio, txtPagoMensual, txtPagoInicial;
    private EditText etxtDescripcion, etxtValorAuto, etxtPorcentajePI;
    private RadioButton rdb12, rdb24, rdb36, rdb48;
    private Button btnCalcular, btnRegresar, btnLimpiar;
    private Cotizacion cot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String descripcion = etxtDescripcion.getText().toString();
                String valorAuto = etxtValorAuto.getText().toString();
                String porcentajePI = etxtPorcentajePI.getText().toString();
                if (descripcion.isEmpty() || valorAuto.isEmpty() || porcentajePI.isEmpty()) {
                    Toast.makeText(CotizacionActivity.this, "Falto CapturarInformación",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    int plazo=0;
                    float pagoInicial=0.0f;
                    float pagoMensual=0.0f;
                    if(rdb12.isChecked())
                        plazo=12;
                    else if(rdb24.isChecked())
                        plazo=24;
                    else if(rdb36.isChecked())
                        plazo=36;
                    else if(rdb48.isChecked())
                        plazo=48;
                    cot.setDescripcion(descripcion.toString());
                    cot.setValorAuto(Float.parseFloat(valorAuto.toString()));
                    cot.setPorEnganche(Float.parseFloat(porcentajePI.toString()));
                    cot.setPlazos(plazo);

                    pagoInicial=cot.calcularPagoInicial();
                    pagoMensual=cot.calcularPagoMensual();
                    txtPagoInicial.setText(String.valueOf("Pago Inicial: $"+pagoInicial));
                    txtPagoMensual.setText(String.valueOf("Pago Mensual: $"+pagoMensual));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etxtDescripcion.setText("");
                etxtValorAuto.setText("");
                etxtPorcentajePI.setText("");
                txtPagoMensual.setText("Pago Mensual: ");
                txtPagoInicial.setText("Pago Inicial: ");
                rdb12.setChecked(true);
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CotizacionActivity.this, cotizacionMainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtFolio = (TextView) findViewById(R.id.txtFolio);
        txtPagoInicial = (TextView) findViewById(R.id.txtPagoInicial);
        txtPagoMensual = (TextView) findViewById(R.id.txtPagoMensual);
        etxtDescripcion = (EditText) findViewById(R.id.etxtDescripcion);
        etxtValorAuto = (EditText) findViewById(R.id.etxtValorAuto);
        etxtPorcentajePI = (EditText) findViewById(R.id.etxtPorcentajePI);
        rdb12 = (RadioButton) findViewById(R.id.rdb12);
        rdb24 = (RadioButton) findViewById(R.id.rdb24);
        rdb36 = (RadioButton) findViewById(R.id.rdb36);
        rdb48 = (RadioButton) findViewById(R.id.rdb48);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        cot = new Cotizacion();
        txtFolio.setText("Folio: "+ String.valueOf(cot.generaId()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente"); 
        txtUsuario.setText("Nombre: "+nombre);
        rdb12.setChecked(true);
    }
}