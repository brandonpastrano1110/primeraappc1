package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class Conversion_grados extends AppCompatActivity {
    private EditText txtGrados;
    private TextView txtResultados;
    private RadioButton rdbCel;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    private float gradosCel, gradosFah, grados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion_grados);
        iniciarComponentes();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        btnCalcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(txtGrados.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"Capturar la cantidad", Toast.LENGTH_SHORT).show();
                }
                else{
                    grados = Float.parseFloat(txtGrados.getText().toString());
                    if(rdbCel.isChecked()){
                        //convertir el cel a fah
                        gradosCel=(grados * 9/5) + 32;
                        txtResultados.setText(String.valueOf(gradosCel));
                    }
                    else{
                        //convertir el fah a cel
                        gradosFah=(grados - 32) * 5/9;
                        txtResultados.setText(String.valueOf(gradosFah));
                    }

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultados.setText("");
                rdbCel.setChecked(true);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Conversion_grados.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    public void iniciarComponentes(){
        txtGrados = (EditText) findViewById(R.id.txtCantidad);
        txtResultados=(TextView) findViewById(R.id.txtResultados);
        rdbCel=(RadioButton) findViewById(R.id.rdbCel);
        btnCalcular=(Button) findViewById(R.id.btnCalcular);
        btnCerrar=(Button) findViewById(R.id.btnCerrar);
        btnLimpiar=(Button) findViewById(R.id.btnLimpiar);

    }
}