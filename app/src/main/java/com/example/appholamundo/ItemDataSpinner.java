package com.example.appholamundo;

public class ItemDataSpinner {
    private String categoria;
    private String descripcion;
    private int imgId;

    public ItemDataSpinner(){
        this.categoria="";
        this.descripcion="";
        this.imgId=0;
    }
    public ItemDataSpinner(String categoria, String descripcion, int imgId) {
        this.categoria = categoria;
        this.descripcion = descripcion;
        this.imgId = imgId;
    }
    public ItemDataSpinner(ItemDataSpinner itemData) {
        this.categoria = itemData.categoria;
        this.descripcion =itemData.descripcion;
        this.imgId = itemData.imgId;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
